FROM node:16-alpine3.11

ARG REACT_APP_IS_STAGING_ARG=false
ENV REACT_APP_IS_STAGING=$REACT_APP_IS_STAGING_ARG

RUN echo "dockerfile arg ${REACT_APP_IS_STAGING_ARG}"
RUN echo "dockerfile var ${REACT_APP_IS_STAGING}"

ENV NODE_ENV="production"

WORKDIR /app

COPY server.js .

COPY createEnvVars.sh .

RUN npm i express

RUN mkdir build

COPY ./build ./build

RUN chmod u+x createEnvVars.sh
RUN ./createEnvVars.sh

EXPOSE 80

CMD ["node", "server"]