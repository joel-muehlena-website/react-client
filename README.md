# Joel Mühlena Client

## Description

This repo contains the whole client for joel.muehlena.de. It includes the view the user sees if he opens the site as well as the admin view for logged in users.
