#!/bin/sh


#touch ./build/envVarWindowBuilder.js

echo "shell file var ${REACT_APP_IS_STAGING}"

if [ "$REACT_APP_IS_STAGING" = "true" ]; then
    echo "Staging is true"
    echo "window.REACT_APP_IS_STAGING = true" > ./build/envVarWindowBuilder.js
else
    echo "Staging is false"
    echo "window.REACT_APP_IS_STAGING = false" > ./build/envVarWindowBuilder.js
fi