import React, { Component, createRef } from "react";
import { Link } from "react-router-dom";

class Layout extends Component<any, {}> {
  private firstMenuPointRef = createRef<HTMLDivElement>();
  private userMenuRef = createRef<HTMLDivElement>();

  state = {
    menuOpen: true,
    showUserMenu: false,
    activePoint: this.firstMenuPointRef.current,
  };

  componentDidMount() {
    document.addEventListener("mousedown", this.checkUserMenu, false);

    const path: string = this.props.location.pathname.split("/")[2];

    if (path === "" || path === undefined) {
      this.props.history.push("/admin/start");
      this.setState({
        activePoint: this.firstMenuPointRef.current,
      });
      this.firstMenuPointRef.current!.classList.add(
        "sidebar__navigation--entry--active"
      );
    } else {
      let element = document.querySelector<HTMLDivElement>(
        `.sidebar__navigation--entry.${path}`
      )!;

      this.setState({
        activePoint: element,
      });

      element.classList.add("sidebar__navigation--entry--active");
    }
  }

  componentWillUnmount() {
    document.removeEventListener("mousedown", this.checkUserMenu, false);
  }

  UNSAFE_componentWillUpdate(nextProps: any, nextState: any) {
    if (nextState.menuOpen || nextState.menuOpen === false) {
      this.openSideBar();
    }
  }

  checkUserMenu = (e: any): void => {
    if (this.userMenuRef.current!.contains(e.target)) {
      return;
    } else {
      this.setState({ showUserMenu: false });
      this.changeUserMenu();
    }
  };

  onMenuBarsClick = async (): Promise<void> => {
    await this.setState({
      menuOpen: !this.state.menuOpen,
    });

    this.openSideBar();
  };

  openSideBar = (): void => {
    if (!this.state.menuOpen) {
      document.querySelector(".dashboard")!.classList.add("dashboard--small");

      document
        .querySelector(".sidebar__topPart")!
        .classList.add("sidebar__topPart--small");

      let allmenuEntries = document.querySelectorAll(
        ".sidebar__navigation--title"
      );
      allmenuEntries.forEach((e) => {
        e.classList.add("sidebar__navigation--title--small");
      });

      let allIcons = document.querySelectorAll(".sidebar__navigation--entry");
      allIcons.forEach((e) => {
        e.classList.add("sidebar__navigation--entry--small");
      });
    } else {
      document
        .querySelector(".dashboard")!
        .classList.remove("dashboard--small");

      document
        .querySelector(".sidebar__topPart")!
        .classList.remove("sidebar__topPart--small");

      let allmenuEntries = document.querySelectorAll(
        ".sidebar__navigation--title"
      );
      allmenuEntries.forEach((e) => {
        e.classList.remove("sidebar__navigation--title--small");
      });

      let allIcons = document.querySelectorAll(".sidebar__navigation--entry");
      allIcons.forEach((e) => {
        e.classList.remove("sidebar__navigation--entry--small");
      });
    }
  };

  changeActiveMenu = (e: any): void => {
    const { activePoint } = this.state;

    activePoint!.classList.remove("sidebar__navigation--entry--active");
    this.setState({ activePoint: e.target.parentElement });
    console.log(e.target);
    e.target.parentElement.classList.add("sidebar__navigation--entry--active");
  };

  onProfileSecClick = async (e: any): Promise<void> => {
    await this.setState({
      showUserMenu: !this.state.showUserMenu,
    });

    this.changeUserMenu();
  };

  changeUserMenu = (): void => {
    if (this.state.showUserMenu) {
      document
        .querySelector(".navigation__profileSec")!
        .classList.add("navigation__profileSec--active");

      (
        document.querySelector(".navigation__profileSec--icon")! as any
      ).style.transform = "rotate(180deg)";

      (
        document.querySelector(".navigation__userMenu")! as any
      ).style.visibility = "visible";
      (document.querySelector(".navigation__userMenu")! as any).style.opacity =
        "1";
      (
        document.querySelector(".navigation__userMenu")! as any
      ).style.transform = "translateX(0)";

      (
        document.querySelector(
          ".navigation__profileSec--notificationCounter"
        )! as any
      ).style.display = "none";

      (
        document.querySelector(
          ".navigation__userMenu--notificationCounter"
        )! as any
      ).style.display = "flex";
    } else {
      document
        .querySelector(".navigation__profileSec")!
        .classList.remove("navigation__profileSec--active");

      (
        document.querySelector(".navigation__profileSec--icon")! as any
      ).style.transform = "rotate(0deg)";
      (
        document.querySelector(".navigation__userMenu")! as any
      ).style.visibility = "hidden";
      (document.querySelector(".navigation__userMenu")! as any).style.opacity =
        "0";
      (
        document.querySelector(".navigation__userMenu")! as any
      ).style.transform = "translateX(120%)";
      (
        document.querySelector(
          ".navigation__profileSec--notificationCounter"
        )! as any
      ).style.display = "flex";
      (
        document.querySelector(
          ".navigation__userMenu--notificationCounter"
        )! as any
      ).style.display = "none";
    }
  };

  render() {
    return (
      <div className="dashboard">
        <aside className="sidebar">
          <div className="sidebar__topPart">
            <h2>Hello, Joel</h2>
            <div className="sidebar__seperator" />
          </div>
          <nav className="sidebar__navigation">
            <div
              className="sidebar__navigation--entry start"
              ref={this.firstMenuPointRef}
            >
              <i className="fa fa-home" />
              <Link
                to="/admin/start"
                className="sidebar__navigation--title"
                onClick={this.changeActiveMenu}
              >
                Start
              </Link>
            </div>

            <div className="sidebar__navigation--entry users">
              <i className="fa fa-user" />
              <Link
                to="/admin/users"
                className="sidebar__navigation--title"
                onClick={this.changeActiveMenu}
              >
                Users
              </Link>
            </div>

            <div className="sidebar__navigation--entry messages">
              <i className="fa fa-envelope">
                <div className="sidebar__navigation--notificationCounter">
                  <span>3</span>
                </div>
              </i>
              <Link
                to="/admin/messages"
                className="sidebar__navigation--title"
                onClick={this.changeActiveMenu}
              >
                Messages
              </Link>
            </div>

            <div className="sidebar__navigation--entry website">
              <i className="fa fa-code-fork" />
              <Link
                to="/admin/website"
                className="sidebar__navigation--title"
                onClick={this.changeActiveMenu}
              >
                Website
              </Link>
            </div>
          </nav>
        </aside>
        <header className="navigation">
          <div
            className="navigation__hamburgermenu"
            onClick={this.onMenuBarsClick}
          >
            <div className="navigation__hamburgermenu--bar" />
            <div className="navigation__hamburgermenu--bar" />
            <div className="navigation__hamburgermenu--bar" />
          </div>

          <div className="navigation__profileSec" ref={this.userMenuRef}>
            <div>
              <img
                alt="Joel Mühlena"
                className="navigation__profileSec--profilePic"
                src={require("../../../img/dev/aboutPic.jpg")}
                onClick={this.onProfileSecClick}
              />
              <div className="navigation__profileSec--notificationCounter">
                <span>3</span>
              </div>
            </div>
            <div onClick={this.onProfileSecClick}>
              <p>Joel Mühlena</p>
            </div>
            <div onClick={this.onProfileSecClick}>
              <i className="fa fa-angle-down navigation__profileSec--icon" />
            </div>
            <div className="navigation__userMenu">
              <div className="navigation__userMenu--entry">
                <Link to="">My account</Link>
              </div>
              <div className="navigation__userMenu--entry">
                <Link to="">Messages</Link>
                <div className="navigation__userMenu--notificationCounter">
                  <span>3</span>
                </div>
              </div>
              <div className="navigation__userMenu--entry">
                <Link to="">Settings</Link>
              </div>
            </div>
          </div>
        </header>
        <div className="dashboard__content">{this.props.children}</div>
      </div>
    );
  }
}

export default Layout;
