import React, { Component, createRef } from "react";
import validator from "validator";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import { login } from "../../../actions/usersActions";
import { setError } from "../../../actions/errorActions";
import { withRouter, WithRouterProps } from "../../common/withRouter";

interface ILoginState {
  name: string;
  nameError: string;
  password: string;
  passwordError: string;
  callback: undefined | string;
}

interface Props {
  login: (userData: any) => void;
  setError: (error: any) => void;
  auth: any;
  errors: any;
}

class Login extends Component<Props & WithRouterProps, ILoginState> {
  private firstInputRef = createRef<HTMLInputElement>();

  state = {
    name: "",
    nameError: "",
    password: "",
    passwordError: "",
    callback: undefined,
  };

  componentDidMount() {
    this.firstInputRef.current!.focus();
    document.title = "Login - Joel Mühlena";

    const callbackUrl = new URLSearchParams(this.props.location.search).get(
      "callback"
    );

    if (callbackUrl !== null) {
      this.setState((state) => ({ callback: callbackUrl }));
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps: any) {
    if (nextProps.auth.isAuthenticated) {
      this.props.navigate("/admin");
    }
  }

  onInputChange = async (e: any): Promise<void> => {
    await this.setState({
      [e.target.name]: e.target.value,
    } as Pick<ILoginState, keyof ILoginState>);

    this.checkForm();
  };

  checkForm = (): void => {
    const { name, password } = this.state;

    const submitButton =
      document.querySelector<HTMLInputElement>(".loginPage__submit")!;

    if (!validator.isEmpty(name) && !validator.isEmpty(password)) {
      submitButton.style.backgroundColor = "#27ae60";
    } else {
      submitButton.style.backgroundColor = "#c0392b";
    }
  };

  onSubmit = (e: any): void => {
    e.preventDefault();

    this.props.setError({ usernameerror: "", passworderror: "" });
    this.setState({
      nameError: "",
      passwordError: "",
    });

    const { name, password } = this.state;

    if (!validator.isEmpty(name) && !validator.isEmpty(password)) {
      this.props.login({
        username: name,
        password,
      });
    } else {
      if (validator.isEmpty(name)) {
        this.setState({
          nameError: "Please enter a username",
        });
      } else {
        this.setState({
          nameError: "",
        });
      }

      if (validator.isEmpty(password)) {
        this.setState({
          passwordError: "Please enter a password",
        });
      } else {
        this.setState({
          passwordError: "",
        });
      }
    }
  };

  render() {
    const { name, password } = this.state;

    return (
      <div className="loginPage">
        <div className="loginPage__container">
          <Link to="/">
            <i className="fa fa-chevron-circle-left loginPage__backIcon" />
          </Link>
          <h1 className="loginPage__heading">Login</h1>
          <form className="loginPage__form" onSubmit={this.onSubmit} noValidate>
            <input
              type="text"
              name="name"
              value={name}
              onChange={this.onInputChange}
              ref={this.firstInputRef}
              placeholder="Username"
              required
            />
            <small className="errorName">
              {this.props.errors.usernameerror
                ? this.props.errors.usernameerror
                : this.state.nameError}
            </small>
            <input
              type="password"
              name="password"
              value={password}
              onChange={this.onInputChange}
              placeholder="Password"
              required
            />
            <small className="errorPassword">
              {this.props.errors.passworderror
                ? this.props.errors.passworderror
                : this.state.passwordError}
            </small>
            <input
              type="submit"
              name="login"
              className="loginPage__submit"
              value="Login"
            />
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    auth: state.user,
    errors: state.errors,
  };
};

export default withRouter(connect(mapStateToProps, { login, setError })(Login));
