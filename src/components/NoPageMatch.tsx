import React, { Component } from 'react'

export default class NoPageMatch extends Component {
  render() {
    return (
      <div>
        404 - Not found
      </div>
    )
  }
}
