import React from "react";
import classNames from "classnames";

interface Props {
  onclick: (e: any) => void;
  active: boolean;
  index: number;
}

const CardIndicator: React.SFC<Props> = ({ active, onclick, index }) => {
  return (
    <div
      className={classNames("cardSlider__cardIndicator--point", {
        "cardSlider__cardIndicator--point--active": active
      })}
      onClick={onclick}
      data-index={index}
    />
  );
};

export default CardIndicator;
