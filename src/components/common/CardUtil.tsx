import React, { Component } from "react";
import { SkillSectionInterface } from "../../interfaces/Skills";
import classNames from "classnames";

interface Props {
  skillSet: SkillSectionInterface;
  active?: boolean;
  onclick?: (e: any) => void;
  index?: number;
}

export default class CardUtil extends Component<Props, {}> {
  render() {
    return (
      <div
        className={classNames("skillsPage__cards-slider-wrapper--card", {
          "skillsPage__cards-slider-wrapper--card--active": this.props.active
        })}
        onClick={this.props.onclick}
        data-index={this.props.index}
      >
        <h2 className="skillsPage__cards-title">{this.props.skillSet.title}</h2>
        <div data-index={this.props.index}>
          <ul>
            {this.props.skillSet.entries.map(entry => (
              <li key={entry.content.entryID}>{entry.content.designation}</li>
            ))}
          </ul>
        </div>
      </div>
    );
  }
}
