import React, { FunctionComponent } from "react";
import { useSelector } from "react-redux";
import { Route, Navigate } from "react-router-dom";
import base64 from "base-64";

interface Props {
  element: any;
  [rest: string]: any;
}

export const PrivateRouteV2: FunctionComponent<Props> = ({
  element,
  rest,
}: any) => {
  const callbackUrl = base64.encode(window.location.href);
  const isAuth = useSelector((state: any) => state.user.isAuthenticated);

  let routeComponent = (props: any) =>
    isAuth ? (
      React.createElement(element, props)
    ) : (
      <Navigate to={`/login?callbackUrl=${callbackUrl}`} />
    );

  return <Route {...rest} element={routeComponent} />;
};
