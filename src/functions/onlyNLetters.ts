const onlyNLetters = (text: string, count: number): string => {
    let newString: string = "";
    for(let i=0; i<count;i++){
        newString += text.charAt(i);
    }
    return newString;
}

export default  onlyNLetters;