export interface AuthInterface {
  isAuthenticated: boolean;
  userInfo: {
    id: string;
    roles: string;
    firstName: string;
    lastName: string;
    username: string;
    avatar: string;
  };
  allUsers: UserInfoInterface[];
}

export interface UserInfoInterface {
  _id: string;
  firstName: string;
  lastName: string;
  roles: string;
  email: string;
  username: string;
}
