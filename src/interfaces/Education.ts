export interface EducationInterface {
    length: number;
    data: Array<educationData>;
}

export interface educationData {
    _id: string;
    schoolName: string,
    schoolLocation: string,
    description: string,
    from: number,
    to: number,
    graduated: number,
    current: boolean
}