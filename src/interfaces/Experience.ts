export interface ExperieceDataInterface {
  _id: string;
  companyName: string;
  companyLocation: string;
  type: number;
  title: string;
  from: Date;
  to: Date;
  current: boolean;
  description: string;
}
