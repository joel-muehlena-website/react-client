export interface ProjectsDataInterface {
  _id: string;
  name: string;
  shortDescription: string;
  description: string;
  showcaseImage: string;
  images: Array<string>;
  from: number;
  to?: number;
  current: boolean;
  urls: Array<string>;
  skills: Array<string>;
}
