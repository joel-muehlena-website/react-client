export interface SkillGroup {
  _id: string;
  skillGroupName: string;
  skills: Array<Skill>;
  createdAt: Date;
  updatedAt: Date;
}

export interface Skill {
  _id?: string;
  skillName: string;
  skillDescription?: string;
  level: number;
}

export interface SkillsInterface {
  _id: string;
  heading: string;
  sections: Array<SkillSectionInterface>;
}

export interface SkillSectionInterface {
  title: string;
  entries: Array<SkillEntryInterface>;
}

export interface SkillEntryInterface {
  content: {
    designation: string;
    entryID: string;
  };
}
