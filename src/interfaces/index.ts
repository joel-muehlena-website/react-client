export interface Message {
  _id: number;
  name: string;
  email: string;
  text: string;
  isRead: boolean;
}

export interface User {
  id: number;
  name: string;
  email: string;
}

export interface EducationInterface {
  length: number;
  data: Array<educationData>;
}

export interface educationData {
  _id: string;
  schoolName: string,
  schoolLocation: string,
  description: string,
  from: number,
  to: number,
  graduated: number,
}