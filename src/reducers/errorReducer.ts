import { SET_ERROR, CLEAR_ERROR } from "../actions/types";

const initialState = {};

// eslint-disable-next-line
export default function (state = initialState, action: any) {
  switch (action.type) {
    case SET_ERROR:
      return action.payload;
    case CLEAR_ERROR:
      return {};
    default:
      return state;
  }
}
