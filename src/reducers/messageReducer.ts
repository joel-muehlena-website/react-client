import { GET_ALLMESSAGES } from "../actions/types";

const initialState = {};

// eslint-disable-next-line
export default function (state = initialState, action: any) {
  switch (action.type) {
    case GET_ALLMESSAGES:
      return action.payload;
    default:
      return state;
  }
}
