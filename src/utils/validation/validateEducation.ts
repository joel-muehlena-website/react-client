import validator from "validator";
import isEmpty from "../../functions/isEmpty";
import { educationData } from "../../interfaces/Education";

export default function(data: educationData) {
  let errors: any = {};

  let schoolName = data.schoolName ? data.schoolName : "";
  let schoolLocation = data.schoolLocation ? data.schoolLocation : "";
  let description = data.description ? data.description : "";
  let from = data.from ? data.from.toString() : "";
  let to = data.to ? data.to.toString : "";
  let graduated = data.graduated ? data.graduated : "";

  //School name
  if (validator.isEmpty(schoolName)) {
    errors.schoolName = "School name is required";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
}
