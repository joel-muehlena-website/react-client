import validator from "validator";
import isEmpty from "../../functions/isEmpty";
import { ExperieceDataInterface } from "../../interfaces/Experience";

export default function(data: ExperieceDataInterface) {
  let errors: any = {};

  return {
    errors,
    isValid: isEmpty(errors)
  };
}
