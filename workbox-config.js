module.exports = {
  globDirectory: "build/",
  globPatterns: ["**/*.{json,ico,png,html,js,css,jpg}"],
  swDest: "build\\service-worker.js",
  swSrc: "src-sw.js",
};
